package vn.laptrinh.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import vn.laptrinh.data.source.local.db.AppDatabase
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DataBaseModule {

    @Provides
    @Singleton
    fun providesDatabase(
        @ApplicationContext context: Context
    ): AppDatabase {
        synchronized(AppDatabase::class.java) {
            return Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "university.db"
            ).fallbackToDestructiveMigration().build()
        }
    }

    @Provides
    @Singleton
    fun providesUniversityDao(
        db: AppDatabase
    ) = db.universityDao

    @Provides
    @Singleton
    fun providesUniversityBoardDao(
        db: AppDatabase
    ) = db.universityBoardDao
}