package vn.laptrinh.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import vn.laptrinh.data.source.local.UniversityLocalDataSourceImpl
import vn.laptrinh.data.source.remote.UniversityRemoteDataSourceImpl
import vn.laptrinh.domain.source.local.UniversityLocalDataSource
import vn.laptrinh.domain.source.remote.UniversityRemoteDataSource

@InstallIn(SingletonComponent::class)
@Module
abstract class DataSourceModule {

    @Binds
    abstract fun provideUniversityRemoteDataSource(
        dataSourceImpl: UniversityRemoteDataSourceImpl
    ): UniversityRemoteDataSource

    @Binds
    abstract fun provideUniversityLocalDataSource(
        dataSourceImpl: UniversityLocalDataSourceImpl
    ): UniversityLocalDataSource
}
