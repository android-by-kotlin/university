package vn.laptrinh.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import vn.laptrinh.presentation.ui.mapper.HomeModelMapper
import vn.laptrinh.presentation.ui.state.HomeUIState
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeModelMapper: HomeModelMapper
) : ViewModel() {

    private val _uiState = MutableStateFlow<HomeUIState>(HomeUIState.Loading)
    val uiState: StateFlow<HomeUIState> = _uiState.asStateFlow()

    fun initUIState() {
        viewModelScope.launch {
            _uiState.update { homeModelMapper.toUIState() }
        }
    }
}