package vn.laptrinh.presentation

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import dagger.hilt.android.AndroidEntryPoint
import vn.laptrinh.presentation.component.UniversityDetailComponent
import vn.laptrinh.presentation.ui.model.UniversityDetailUIModel
import vn.laptrinh.presentation.ui.theme.UniversityTheme

@AndroidEntryPoint
class UniversityDetailActivity : ComponentActivity() {

    private val viewModel: UniversityDetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        val bundle = intent.extras
        val universityName = bundle?.getString(UNIVERSITY_NAME).orEmpty()
        viewModel.initUIState(universityName)
        setContent {
            UniversityTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                    val uiModel = viewModel.uiState.collectAsStateWithLifecycle(lifecycleOwner = LocalLifecycleOwner.current).value ?: return@Scaffold
                    ScreenContainer(
                        modifier = Modifier.padding(innerPadding),
                        uiModel = uiModel,
                    )
                }
            }
        }
    }
}

@Composable
fun ScreenContainer(
    modifier: Modifier = Modifier,
    uiModel: UniversityDetailUIModel,
) {
    val activity = LocalContext.current as Activity
    UniversityDetailComponent(
        modifier = modifier,
        uiModel = uiModel,
        onChannelLinkClick = {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/@ChuyenlaptrinhAndroidKotlin")
            )
            activity.startActivity(intent)
        },
        onWebLinkClick = {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://developer.android.com/")
            )
            activity.startActivity(intent)
        }
    )
}