package vn.laptrinh.presentation.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.rounded.KeyboardArrowRight
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import vn.laptrinh.presentation.ui.model.TextUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme
import vn.laptrinh.presentation.UnitFunction

@Composable
fun UniversityComponent(
    modifier: Modifier = Modifier,
    uiModel: TextUIModel,
    onClick: UnitFunction
) {
    Card(
        modifier = modifier.fillMaxWidth(),
        shape = RoundedCornerShape(8.dp),
        elevation = CardDefaults.cardElevation(8.dp),
        colors = CardDefaults.cardColors(
            containerColor = ColorByTheme(
                light = Color.White,
                dark = Color.DarkGray
            )()
        ),
        onClick = onClick
    ) {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .padding(vertical = 16.dp)
        ) {
            Spacer(modifier = Modifier.width(16.dp))
            Text(
                modifier = Modifier
                    .width(0.dp)
                    .weight(1f),
                text = uiModel.asString(),
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold,
                color = ColorByTheme(
                    light = Color.Black,
                    dark = Color.White
                )()
            )
            Icon(
                imageVector = Icons.AutoMirrored.Rounded.KeyboardArrowRight,
                tint = ColorByTheme(
                    light = Color.Black,
                    dark = Color.White
                )(),
                contentDescription = "",
            )
            Spacer(modifier = Modifier.width(8.dp))
        }
    }
}