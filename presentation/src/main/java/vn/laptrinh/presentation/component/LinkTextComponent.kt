package vn.laptrinh.presentation.component

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.ClickableText
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Gray
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import vn.laptrinh.presentation.annotation.CombinedPreviews
import vn.laptrinh.presentation.emptyFunction
import vn.laptrinh.presentation.ui.model.LinkTextUIModel
import vn.laptrinh.presentation.ui.model.TextUIModel
import vn.laptrinh.university.presentation.R

private const val LINK = "LINK"

@Composable
fun LinkTextComponent(
    modifier: Modifier = Modifier,
    uiModel: LinkTextUIModel,
    textStyle: TextStyle = TextStyle.Default,
    linkTextColor: Color,
    linkTextDecoration: TextDecoration = TextDecoration.Underline,
    fontSize: TextUnit = TextUnit.Unspecified,
) {
    val annotatedString = buildAnnotatedString {
        val fullText = uiModel.fullText.asString()
        append(fullText)
        addStyle(
            style = SpanStyle(
                fontSize = fontSize,
                fontWeight = FontWeight.Medium,
                fontStyle = FontStyle.Italic
            ),
            start = 0,
            end = fullText.length
        )
        for (linkTextUIModel in uiModel.functionByTexts.keys) {
            val linkText = linkTextUIModel.asString()
            val startIndex = fullText.indexOf(linkText)
            val endIndex = startIndex + linkText.length
            addStyle(
                style = SpanStyle(
                    color = linkTextColor,
                    fontSize = fontSize,
                    textDecoration = linkTextDecoration,
                    fontWeight = FontWeight.ExtraBold
                ),
                start = startIndex,
                end = endIndex
            )
            addStringAnnotation(
                tag = LINK,
                annotation = linkText,
                start = startIndex,
                end = endIndex
            )
        }
    }

    val onClickFunctionByStrings = uiModel.functionByTexts.map {
        it.key.asString() to it.value
    }.toMap()

    ClickableText(
        modifier = modifier,
        text = annotatedString,
        style = textStyle,
        onClick = { charIndex ->
            val annotationRange = annotatedString.getStringAnnotations(
                tag = LINK,
                start = charIndex,
                end = charIndex
            ).firstOrNull()
            println("LinkTextComponent: Click: Character index: $charIndex, annotationRange: $annotationRange")
            annotationRange?.let { stringRange ->
                val function = onClickFunctionByStrings[stringRange.item]
                println("LinkTextComponent: function: $function")
                function?.invoke()
            }
        }
    )
}

@CombinedPreviews
@Composable
private fun PreviewLinkTextComponent() {
    LinkTextComponent(
        modifier = Modifier.fillMaxWidth(),
        uiModel = LinkTextUIModel(
            fullText = TextUIModel.StringResource(R.string.footer_full_text),
            functionByTexts = mapOf(
                TextUIModel.StringResource(R.string.dev_android_by_kotlin) to emptyFunction,
                TextUIModel.StringResource(R.string.android_of_google) to emptyFunction
            )
        ),
        textStyle = TextStyle(
            textAlign = TextAlign.Center,
            color = Gray
        ),
        linkTextColor = Color.Blue,
        fontSize = 18.sp
    )
}