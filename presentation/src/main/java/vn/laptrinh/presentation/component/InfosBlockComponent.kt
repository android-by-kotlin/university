package vn.laptrinh.presentation.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import vn.laptrinh.presentation.ui.model.InfosBlockUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme

@Composable
fun InfosBlockComponent(
    modifier: Modifier = Modifier,
    uiModel: InfosBlockUIModel
) {
    Column(
        modifier = modifier.fillMaxWidth()
    ) {
        Text(
            text = uiModel.title.asString(),
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            color = ColorByTheme(
                light = Color.Black,
                dark = Color.White
            )()
        )
        Spacer(modifier = Modifier.height(4.dp))
        HorizontalDivider(
            modifier = Modifier
                .width(40.dp)
                .height(2.dp)
        )
        uiModel.infos.forEach {
            Spacer(modifier = Modifier.height(16.dp))
            InfoComponent(uiModel = it)
        }
    }
}