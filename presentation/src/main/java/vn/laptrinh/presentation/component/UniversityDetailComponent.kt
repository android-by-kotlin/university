package vn.laptrinh.presentation.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import vn.laptrinh.presentation.UnitFunction
import vn.laptrinh.presentation.ui.model.LinkTextUIModel
import vn.laptrinh.presentation.ui.model.TextUIModel
import vn.laptrinh.presentation.ui.model.UniversityDetailUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme
import vn.laptrinh.university.presentation.R

@Composable
fun UniversityDetailComponent(
    modifier: Modifier = Modifier,
    uiModel: UniversityDetailUIModel,
    onChannelLinkClick: UnitFunction,
    onWebLinkClick: UnitFunction
) {
    WithFooterComponent(
        uiModel = LinkTextUIModel(
            fullText = TextUIModel.StringResource(R.string.footer_full_text),
            functionByTexts = mapOf(
                TextUIModel.StringResource(R.string.dev_android_by_kotlin) to onChannelLinkClick,
                TextUIModel.StringResource(R.string.android_of_google) to onWebLinkClick
            )
        )
    ) {
        Column(
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .fillMaxSize()
        ) {
            Column(
                modifier = modifier.fillMaxWidth()
            ) {
                Spacer(modifier = Modifier.height(16.dp))
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = uiModel.name.asString(),
                    textAlign = TextAlign.Center,
                    fontSize = 36.sp,
                    fontWeight = FontWeight.Bold,
                    color = ColorByTheme(
                        light = Color.Black,
                        dark = Color.White
                    )()
                )
            }
            Spacer(modifier = Modifier.height(16.dp))
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                InfosBlockComponent(uiModel = uiModel.commonInfos)
                Spacer(modifier = Modifier.height(24.dp))
                InfosBlockComponent(uiModel = uiModel.boardInfos)
            }
        }
    }
}