package vn.laptrinh.presentation.component

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import vn.laptrinh.presentation.UnitFunction
import vn.laptrinh.presentation.ui.model.HomeUIModel
import vn.laptrinh.presentation.ui.model.LinkTextUIModel
import vn.laptrinh.presentation.ui.model.TextUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme
import vn.laptrinh.presentation.ui.theme.ResByTheme
import vn.laptrinh.university.presentation.R

@Composable
fun HomeComponent(
    modifier: Modifier = Modifier,
    uiModel: HomeUIModel,
    onUniversityClick: (universityName: String) -> Unit,
    onChannelLinkClick: UnitFunction,
    onWebLinkClick: UnitFunction
) {
    WithFooterComponent(
        uiModel = LinkTextUIModel(
            fullText = TextUIModel.StringResource(R.string.footer_full_text),
            functionByTexts = mapOf(
                TextUIModel.StringResource(R.string.dev_android_by_kotlin) to onChannelLinkClick,
                TextUIModel.StringResource(R.string.android_of_google) to onWebLinkClick
            )
        )
    ) {
        Column(
            modifier = modifier
                .fillMaxSize()
                .padding(horizontal = 16.dp)
        ) {
            Spacer(modifier = Modifier.height(16.dp))
            Image(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .height(150.dp),
                contentScale = ContentScale.Inside,
                painter = painterResource(
                    ResByTheme(
                        light = R.drawable.ic_school_dark_blue,
                        dark = R.drawable.ic_school_light_blue
                    )()
                ),
                contentDescription = null
            )
            Spacer(modifier = Modifier.height(16.dp))
            Column(
                modifier = modifier
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                Text(
                    text = "Các trường đại học",
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold,
                    color = ColorByTheme(
                        light = Color.Black,
                        dark = Color.White
                    )()
                )
                Spacer(modifier = Modifier.height(4.dp))
                HorizontalDivider(
                    modifier = Modifier
                        .width(40.dp)
                        .height(2.dp)
                )
                uiModel.universities.forEach {
                    val universityName = it.asString()
                    Spacer(modifier = Modifier.height(16.dp))
                    UniversityComponent(
                        uiModel = it,
                        onClick = { onUniversityClick(universityName) }
                    )
                }
                Spacer(modifier = Modifier.height(16.dp))
            }
        }
    }
}