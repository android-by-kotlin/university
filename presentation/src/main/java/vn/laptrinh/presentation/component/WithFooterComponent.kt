package vn.laptrinh.presentation.component

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import vn.laptrinh.presentation.UnitFunction
import vn.laptrinh.presentation.ui.model.LinkTextUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme

@Composable
fun WithFooterComponent(
    modifier: Modifier = Modifier,
    uiModel: LinkTextUIModel,
    content: @Composable UnitFunction,
) {
    Column(modifier = modifier.fillMaxSize()) {
        Box(
            modifier = modifier
                .weight(1f)
                .height(0.dp)
        ) {
            content()
        }

        // Footer
        LinkTextComponent(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            uiModel = uiModel,
            linkTextColor = ColorByTheme(
                light = Color(0xFF00008B),
                dark = Color(0xFF45B6FE)
            )(),
            textStyle = TextStyle(
                textAlign = TextAlign.Center,
                color = ColorByTheme(
                    light = Color.Black,
                    dark = Color.White
                )(),
                lineHeight = 20.sp
            ),
            fontSize = 13.sp
        )
    }
}