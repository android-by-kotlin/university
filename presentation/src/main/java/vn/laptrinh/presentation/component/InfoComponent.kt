package vn.laptrinh.presentation.component

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import vn.laptrinh.presentation.ui.model.InfoUIModel
import vn.laptrinh.presentation.ui.theme.ColorByTheme

@Composable
fun InfoComponent(
    modifier: Modifier = Modifier,
    uiModel: InfoUIModel
) {
    Row(modifier = modifier.fillMaxWidth()) {
        Text(
            modifier = Modifier
                .width(0.dp)
                .weight(1f),
            text = uiModel.title.asString(),
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = ColorByTheme(
                light = Color.Black,
                dark = Color.White
            )()
        )
        Spacer(modifier = Modifier.width(16.dp))
        Text(
            text = uiModel.value.asString(),
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            color = ColorByTheme(
                light = Color(0xFF00008B),
                dark = Color(0xFF45B6FE)
            )()
        )
    }
}