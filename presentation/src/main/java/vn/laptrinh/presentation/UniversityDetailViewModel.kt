package vn.laptrinh.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import vn.laptrinh.presentation.ui.mapper.UniversityDetailModelMapper
import vn.laptrinh.presentation.ui.model.UniversityDetailUIModel
import javax.inject.Inject

@HiltViewModel
class UniversityDetailViewModel @Inject constructor(
    private val universityDetailModelMapper: UniversityDetailModelMapper
) : ViewModel() {

    private val _uiState = MutableStateFlow<UniversityDetailUIModel?>(null)
    val uiState: StateFlow<UniversityDetailUIModel?> = _uiState.asStateFlow()

    fun initUIState(
        universityName: String
    ) {
        viewModelScope.launch {
            _uiState.update { universityDetailModelMapper.toUIState(universityName) }
        }
    }
}