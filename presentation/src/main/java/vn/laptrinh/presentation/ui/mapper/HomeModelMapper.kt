package vn.laptrinh.presentation.ui.mapper

import vn.laptrinh.domain.usecase.GetUniversitiesUseCase
import vn.laptrinh.presentation.ui.model.HomeUIModel
import vn.laptrinh.presentation.ui.model.TextUIModel
import vn.laptrinh.presentation.ui.state.HomeUIState
import javax.inject.Inject

class HomeModelMapper @Inject constructor(
    private val getUniversitiesUseCase: GetUniversitiesUseCase
) {

    suspend fun toUIState() = HomeUIState.Done(
        uiModel = HomeUIModel(
            universities = getUniversitiesUseCase().map {
                TextUIModel.DynamicString(it.name)
            }
        )
    )
}