package vn.laptrinh.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class UniversityDetailUIModel(
    val name: TextUIModel,
    val commonInfos: InfosBlockUIModel,
    val boardInfos: InfosBlockUIModel
)
