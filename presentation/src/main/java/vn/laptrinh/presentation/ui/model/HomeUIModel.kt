package vn.laptrinh.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class HomeUIModel(
    val universities: List<TextUIModel>
)