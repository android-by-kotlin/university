package vn.laptrinh.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class InfoUIModel(
    val title: TextUIModel,
    val value: TextUIModel
)
