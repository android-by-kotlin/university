package vn.laptrinh.presentation.ui.model

import androidx.compose.runtime.Immutable

@Immutable
data class InfosBlockUIModel(
    val title: TextUIModel,
    val infos: List<InfoUIModel>
)
