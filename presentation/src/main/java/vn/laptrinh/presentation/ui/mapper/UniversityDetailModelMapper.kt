package vn.laptrinh.presentation.ui.mapper

import vn.laptrinh.domain.usecase.GetUniversityByNameUseCase
import vn.laptrinh.presentation.ui.model.InfoUIModel
import vn.laptrinh.presentation.ui.model.InfosBlockUIModel
import vn.laptrinh.presentation.ui.model.TextUIModel
import vn.laptrinh.presentation.ui.model.UniversityDetailUIModel
import javax.inject.Inject

class UniversityDetailModelMapper @Inject constructor(
    private val getUniversityByNameUseCase: GetUniversityByNameUseCase
) {

    suspend fun toUIState(
        universityName: String
    ): UniversityDetailUIModel? {
        return getUniversityByNameUseCase(universityName)?.let {
            UniversityDetailUIModel(
                name = TextUIModel.DynamicString(it.name),
                commonInfos = InfosBlockUIModel(
                    title = TextUIModel.DynamicString("Thông tin chung"),
                    infos = listOf(
                        InfoUIModel(
                            title = TextUIModel.DynamicString("Tổng số sinh viên"),
                            value = TextUIModel.DynamicString(it.numberStudents.toString())
                        )
                    )
                ),
                boardInfos = InfosBlockUIModel(
                    title = TextUIModel.DynamicString("Ban giám hiệu"),
                    infos = listOf(
                        InfoUIModel(
                            title = TextUIModel.DynamicString("Điện thoại"),
                            value = TextUIModel.DynamicString(it.board.phone)
                        ),
                        InfoUIModel(
                            title = TextUIModel.DynamicString("Email"),
                            value = TextUIModel.DynamicString(it.board.email)
                        )
                    )
                )
            )
        }
    }
}