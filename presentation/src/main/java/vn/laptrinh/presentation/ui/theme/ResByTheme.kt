package vn.laptrinh.presentation.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable

@Immutable
data class ResByTheme(
    val light: Int,
    val dark: Int
) {
    constructor(
        res: Int
    ) : this(
        light = res,
        dark = res
    )

    @Composable
    operator fun invoke(): Int {
        return if (isSystemInDarkTheme()) dark else light
    }
}