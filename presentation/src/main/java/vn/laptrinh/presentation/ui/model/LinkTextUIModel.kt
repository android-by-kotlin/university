package vn.laptrinh.presentation.ui.model

import androidx.compose.runtime.Immutable
import vn.laptrinh.presentation.UnitFunction

@Immutable
data class LinkTextUIModel(
    val fullText: TextUIModel,
    val functionByTexts: Map<TextUIModel, UnitFunction>,
)