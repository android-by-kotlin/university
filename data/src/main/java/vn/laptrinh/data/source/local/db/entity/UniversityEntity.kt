package vn.laptrinh.data.source.local.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UniversityEntity(
    @PrimaryKey val name: String,
    val numberStudents: Int
)
