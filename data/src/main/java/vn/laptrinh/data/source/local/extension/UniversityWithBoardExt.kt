package vn.laptrinh.data.source.local.extension

import vn.laptrinh.data.source.local.db.relation.UniversityWithBoard
import vn.laptrinh.domain.model.University
import vn.laptrinh.domain.model.UniversityBoard

fun UniversityWithBoard.toDomain(
    universityName: String
) = University(
    name = universityName,
    numberStudents = universityEntity.numberStudents,
    board = UniversityBoard(
        phone = boardEntity.phone,
        email = boardEntity.email
    )
)