package vn.laptrinh.data.source.local.db.relation

import androidx.room.Embedded
import androidx.room.Relation
import vn.laptrinh.data.source.local.db.entity.UniversityBoardEntity
import vn.laptrinh.data.source.local.db.entity.UniversityEntity

data class UniversityWithBoard(

    @Embedded
    val universityEntity: UniversityEntity,

    @Relation(
        parentColumn = "name",
        entityColumn = "universityName"
    )
    val boardEntity: UniversityBoardEntity
)