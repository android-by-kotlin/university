package vn.laptrinh.data.source.remote

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import vn.laptrinh.domain.model.University
import vn.laptrinh.domain.model.UniversityBoard
import vn.laptrinh.domain.source.remote.UniversityRemoteDataSource
import javax.inject.Inject

class UniversityRemoteDataSourceImpl @Inject constructor(
    private val dispatcher: CoroutineDispatcher
) : UniversityRemoteDataSource {

    override suspend fun getUniversities(): List<University> {
        return withContext(dispatcher) {
            delay(2000)
            listOf(
                University(
                    name = "Đại học Kinh tế",
                    numberStudents = 10000,
                    board = UniversityBoard(
                        phone = "+84 11111111",
                        email = "dhkt@gmail.com"
                    )
                ),
                University(
                    name = "Đại học Công nghệ",
                    numberStudents = 20000,
                    board = UniversityBoard(
                        phone = "+84 22222222",
                        email = "dhcn@gmail.com"
                    )
                )
            )
        }
    }
}