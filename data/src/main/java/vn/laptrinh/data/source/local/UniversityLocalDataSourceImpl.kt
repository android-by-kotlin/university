package vn.laptrinh.data.source.local

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import vn.laptrinh.data.source.local.db.dao.UniversityBoardDao
import vn.laptrinh.data.source.local.db.dao.UniversityDao
import vn.laptrinh.data.source.local.extension.toDomain
import vn.laptrinh.data.source.local.extension.toEntity
import vn.laptrinh.domain.model.University
import vn.laptrinh.domain.source.local.UniversityLocalDataSource
import javax.inject.Inject

class UniversityLocalDataSourceImpl @Inject constructor(
    private val dispatcher: CoroutineDispatcher,
    private val universityDao: UniversityDao,
    private val universityBoardDao: UniversityBoardDao
) : UniversityLocalDataSource {
    override suspend fun saveUniversities(universities: List<University>) {
        withContext(dispatcher) {
            universityDao.insertAll(
                universityEntities = universities.map { it.toEntity() }
            )
            var baseId = 1
            universityBoardDao.insertAll(
                universityBoardEntities = universities.map {
                    it.board.toEntity(
                        id = baseId++,
                        universityName = it.name
                    )
                }
            )
        }
    }

    override suspend fun findByName(universityName: String): University? {
        return withContext(dispatcher) {
            universityDao.findByName(universityName)?.toDomain(universityName)
        }
    }
}