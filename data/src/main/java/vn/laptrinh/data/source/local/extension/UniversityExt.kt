package vn.laptrinh.data.source.local.extension

import vn.laptrinh.data.source.local.db.entity.UniversityEntity
import vn.laptrinh.domain.model.University

fun University.toEntity() = UniversityEntity(
    name = name,
    numberStudents = numberStudents
)