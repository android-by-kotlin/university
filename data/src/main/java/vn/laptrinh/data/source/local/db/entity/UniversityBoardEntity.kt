package vn.laptrinh.data.source.local.db.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = UniversityEntity::class,
            parentColumns = ["name"],
            childColumns = ["universityName"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("universityName")
    ]
)
data class UniversityBoardEntity(
    @PrimaryKey val id: Int,
    val universityName: String,
    val phone: String,
    val email: String
)
