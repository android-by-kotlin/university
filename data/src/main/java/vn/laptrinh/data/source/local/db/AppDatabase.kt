package vn.laptrinh.data.source.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import vn.laptrinh.data.source.local.db.dao.UniversityBoardDao
import vn.laptrinh.data.source.local.db.dao.UniversityDao
import vn.laptrinh.data.source.local.db.entity.UniversityBoardEntity
import vn.laptrinh.data.source.local.db.entity.UniversityEntity

@Database(
    entities = [
        UniversityEntity::class,
        UniversityBoardEntity::class
    ],
    version = 1,
    exportSchema = true
)

abstract class AppDatabase : RoomDatabase() {
    abstract val universityDao: UniversityDao
    abstract val universityBoardDao: UniversityBoardDao
}