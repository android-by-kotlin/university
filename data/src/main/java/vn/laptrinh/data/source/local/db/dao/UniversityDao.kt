package vn.laptrinh.data.source.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import vn.laptrinh.data.source.local.db.entity.UniversityEntity
import vn.laptrinh.data.source.local.db.relation.UniversityWithBoard

@Dao
interface UniversityDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(universityEntities: List<UniversityEntity>)

    @Query("SELECT * FROM UniversityEntity WHERE name = :name LIMIT 1")
    fun findByName(name: String): UniversityWithBoard?
}