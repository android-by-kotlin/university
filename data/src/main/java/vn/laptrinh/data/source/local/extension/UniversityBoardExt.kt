package vn.laptrinh.data.source.local.extension

import vn.laptrinh.data.source.local.db.entity.UniversityBoardEntity
import vn.laptrinh.domain.model.UniversityBoard

fun UniversityBoard.toEntity(
    id: Int,
    universityName: String
) = UniversityBoardEntity(
    id = id,
    universityName = universityName,
    phone = phone,
    email = email
)