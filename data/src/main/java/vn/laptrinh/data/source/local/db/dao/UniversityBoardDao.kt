package vn.laptrinh.data.source.local.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Transaction
import vn.laptrinh.data.source.local.db.entity.UniversityBoardEntity

@Dao
interface UniversityBoardDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(universityBoardEntities: List<UniversityBoardEntity>)
}