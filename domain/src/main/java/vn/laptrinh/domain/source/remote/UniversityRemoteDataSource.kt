package vn.laptrinh.domain.source.remote

import vn.laptrinh.domain.model.University

interface UniversityRemoteDataSource {

    suspend fun getUniversities(): List<University>
}