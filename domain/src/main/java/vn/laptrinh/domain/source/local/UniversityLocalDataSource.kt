package vn.laptrinh.domain.source.local

import vn.laptrinh.domain.model.University

interface UniversityLocalDataSource {

    suspend fun saveUniversities(universities: List<University>)

    suspend fun findByName(universityName: String): University?
}