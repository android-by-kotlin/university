package vn.laptrinh.domain.usecase

import vn.laptrinh.domain.model.University
import vn.laptrinh.domain.source.local.UniversityLocalDataSource
import vn.laptrinh.domain.source.remote.UniversityRemoteDataSource
import javax.inject.Inject

class GetUniversitiesUseCase @Inject constructor(
    private val universityRemoteDataSource: UniversityRemoteDataSource,
    private val universityLocalDataSource: UniversityLocalDataSource
) {

    suspend operator fun invoke(): List<University> {
        val universities = universityRemoteDataSource.getUniversities()
        universityLocalDataSource.saveUniversities(universities)
        return universities
    }
}