package vn.laptrinh.domain.usecase

import vn.laptrinh.domain.model.University
import vn.laptrinh.domain.source.local.UniversityLocalDataSource
import javax.inject.Inject

class GetUniversityByNameUseCase @Inject constructor(
    private val universityLocalDataSource: UniversityLocalDataSource
) {

    suspend operator fun invoke(
        universityName: String
    ): University? = universityLocalDataSource.findByName(universityName)
}