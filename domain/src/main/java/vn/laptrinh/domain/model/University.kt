package vn.laptrinh.domain.model

data class University(
    val name: String,
    val numberStudents: Int,
    val board: UniversityBoard
)