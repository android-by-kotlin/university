package vn.laptrinh.domain.model

data class UniversityBoard(
    val phone: String,
    val email: String
)